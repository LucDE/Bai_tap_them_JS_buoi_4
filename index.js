//Bài 4.1: Nhập ngày tháng năm, xuất ra màn hình ngày trước đó, ngày tiếp theo
var time_input;
var get_date;
var get_month;
var get_year;
var get_new_day;

function show_next_day() {
  time_input = dom_id_value("date");
  get_date = Number(time_input.substr(8, 2));
  get_month = Number(time_input.substr(5, 2));
  get_year = Number(time_input.substr(0, 4));
  get_new_day = get_date + 1;
  put_text_into_id(
    "alert4_1",
    "Ngày " + get_new_day + " tháng " + get_month + " năm " + get_year
  );
}
function show_previous_day() {
  time_input = dom_id_value("date");
  get_date = Number(time_input.substr(8, 2));
  get_month = Number(time_input.substr(5, 2));
  get_year = Number(time_input.substr(0, 4));
  get_new_day = get_date - 1;
  put_text_into_id(
    "alert4_1",
    "Ngày " + get_new_day + " tháng " + get_month + " năm " + get_year
  );
}

//Bài 4.2: Cho biết số ngày của một tháng
var take_year = 0;
var take_month = 0;
var select;
var date_of_take_month;

function dates_of_month() {
  select = dom_id("month");
  //lấy giá trị được chọn của select
  take_month = Number(select.options[select.selectedIndex].value);
  take_year = Number(dom_id_value("input_year"));
  if (
    take_month == 1 ||
    take_month == 3 ||
    take_month == 5 ||
    take_month == 7 ||
    take_month == 8 ||
    take_month == 10 ||
    take_month == 12
  ) {
    date_of_take_month = 31;
  } else if (
    take_month == 4 ||
    take_month == 6 ||
    take_month == 9 ||
    take_month == 11
  ) {
    date_of_take_month = 30;
    //month = 2;
  } else {
    if (take_year % 100 != 0 && take_year % 4 == 0) {
      date_of_take_month = 29;
    } else if (take_year % 100 == 0 && take_year % 400 == 0) {
      date_of_take_month = 29;
    } else {
      date_of_take_month = 28;
    }
  }
  put_text_into_id(
    "alert4_2",
    "Số ngày của tháng " +
      take_month +
      " năm " +
      take_year +
      " là: " +
      date_of_take_month
  );
  return dom_id_value_return_0("input_year");
}
//Bài 4.3: Cách đọc 3 số nguyên
var three_index_number;
var first_number;
var second_number;
var third_number;
var first_number_name;
var second_number_name;
var third_number_name;

function read_three_index() {
  three_index_number = dom_id_value("three_index_number");
  first_number = parseInt(three_index_number / 100);
  second_number = parseInt(three_index_number / 10) % 10;
  third_number = three_index_number % 10;
  first_number_name = return_name_of_number(first_number);
  second_number_name = return_name_of_number(second_number);
  third_number_name = return_name_of_number(third_number);
  if (three_index_number > 99 && three_index_number < 1000) {
    if (three_index_number % 100 == 0) {
      put_text_into_id("alert4_3", first_number_name + " trăm ");
    } else if (three_index_number % 100 == 1) {
      put_text_into_id(
        "alert4_3",
        first_number_name + " trăm lẻ " + third_number_name
      );
    } else if (three_index_number % 100 > 1 && three_index_number % 10 == 1) {
      put_text_into_id(
        "alert4_3",
        first_number_name + " trăm " + second_number_name + " mốt"
      );
    } else if (three_index_number % 100 < 10) {
      put_text_into_id(
        "alert4_3",
        first_number_name + " trăm lẻ " + third_number_name
      );
    } else if (three_index_number % 100 > 1 && three_index_number % 10 == 0) {
      put_text_into_id(
        "alert4_3",
        first_number_name + " trăm " + second_number_name
      );
    } else {
      put_text_into_id(
        "alert4_3",
        first_number_name +
          " trăm " +
          second_number_name +
          " mươi " +
          third_number_name
      );
    }
  } else {
    put_text_into_id("alert4_3", `Vui lòng nhập đúng.`);
  }
  return (three_index_number = 0), dom_id_value_return_0("three_index_number");
}

// Bài 4.4: Tìm tên sinh viên xa trường nhất
const ten_sv1 = dom_id("ten_sv1");
const ten_sv2 = dom_id("ten_sv2");
const ten_sv3 = dom_id("ten_sv3");
const x_sv1 = dom_id("x_sv1");
const x_sv2 = dom_id("x_sv2");
const x_sv3 = dom_id("x_sv3");
const y_sv1 = dom_id("x_sv1");
const y_sv2 = dom_id("x_sv2");
const y_sv3 = dom_id("x_sv3");
const x_school = dom_id("x_school");
const y_school = dom_id("y_school");
function findNeareastStudent() {
  ten_sv1_input = ten_sv1.value;
  ten_sv2_input = ten_sv2.value;
  ten_sv3_input = ten_sv3.value;
  x_sv1_input = x_sv1.value;
  x_sv2_input = x_sv2.value;
  x_sv3_input = x_sv3.value;
  y_sv1_input = y_sv1.value;
  y_sv2_input = y_sv2.value;
  y_sv3_input = y_sv3.value;
  x_school_input = x_school.value;
  y_school_input = y_school.value;
  var distance_sv1 = calculateDistance(
    x_sv1_input,
    y_sv1_input,
    x_school_input,
    y_school_input
  );
  var distance_sv2 = calculateDistance(
    x_sv2_input,
    y_sv2_input,
    x_school_input,
    y_school_input
  );
  var distance_sv3 = calculateDistance(
    x_sv3_input,
    y_sv3_input,
    x_school_input,
    y_school_input
  );
  var array_distance = [distance_sv1, distance_sv2, distance_sv3];
  array_distance_arrange = array_distance.sort(function (a, b) {
    return a - b;
  });
  if (array_distance_arrange[0] === distance_sv1)
    put_text_into_id("alert4_4", `Sinh viên ${ten_sv1_input} gần trường nhất`);
  else if (array_distance_arrange[0] === distance_sv2) {
    put_text_into_id("alert4_4", `Sinh viên ${ten_sv2_input} gần trường nhất`);
  } else {
    put_text_into_id("alert4_4", `Sinh viên ${ten_sv3_input} gần trường nhất`);
  }
}
function calculateDistance(x, y, x1, y1) {
  return Math.sqrt(Math.pow(x1 - x, 2) + Math.pow(y1 - y, 2));
}
