// DOM tới thẻ ID
function dom_id(id) {
  return document.getElementById(id);
}
// Lấy giá trị của ID
function dom_id_value(id) {
  var tagID = document.getElementById(id);
  return tagID.value;
}
// Trả giá trị về 0
function dom_id_value_return_0(id) {
  var tagID = document.getElementById(id);
  return (tagID.value = "");
}
// Thêm item vào cuối mảng
function add_item_to_end(array_A, item) {
  var array_A;
  var item;
  return (array_A = array_A.push(Number(item)));
}
// Thêm nội dung vào bên trong thẻ ID
// Note: ID phải cho vào dấu  nháy
function put_text_into_id(id, text_content) {
  return (dom_id(id).innerHTML = text_content);
}
// Sắp xếp số nhập vào theo thứ tự tăng dần
function arrange_min_to_max(array, new_arange_array) {
  var a = 0;
  // không khai báo lại giá trị phía trên hàm, chỉ lấy giá trị vào từ return
  //   var array = []; sai
  //   var new_arange_array = []; sai
  // không nên đặt các biến giống với biến của bài chính
  var number = 0;
  var min = 0;
  number = array.length;
  for (a = 0; a < number; a++) {
    min = array[0];
    for (i = 0; i < array.length; i++) {
      if (array[i] < min) {
        min = array[i];
      }
    }
    new_arange_array.push(Number(min));
    array.splice(array.indexOf(Number(min)), 1);
  }
  return new_arange_array;
}

// hàm lấy giá trị (number) của người dùng nhập cho vào mảng
function put_number_into_array(id, array__of__number) {
  var num__inp;
  num__inp = dom_id_value(id);
  add_item_to_end(array__of__number, num__inp);
  return array__of__number;
}

// tạo một hàm lấy số sau đó đổi thành chữ
function return_name_of_number(aa) {
  switch (aa) {
    case 1:
      return "một";
    case 2:
      return "hai";
    case 3:
      return "ba";
    case 4:
      return "bốn";
    case 5:
      return "năm";
    case 6:
      return "sáu";
    case 7:
      return "bảy";
    case 8:
      return "tám";
    case 9:
      return "chín";
    default:
      return "không";
  }
}
